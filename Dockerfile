FROM ubuntu:24.04

# Install tools
RUN apt update -y && \
    apt install -y g++-14 clang-18 clang-tidy-18 clang-tools-18 clang-format-18 curl tar git zip unzip make autoconf libtool python3-jinja2 && \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-14 10 && \
    update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-14 10 && \
    update-alternatives --install /usr/bin/cc cc /usr/bin/gcc 20 && \
    update-alternatives --set cc /usr/bin/gcc && \
    update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++ 20 && \
    update-alternatives --set c++ /usr/bin/g++

# Install Qt dependencies
RUN apt install -y '^libxcb.*-dev' libfontconfig1-dev libfreetype6-dev libx11-dev libxext-dev libxfixes-dev libgl1-mesa-dev libglu1-mesa-dev libegl1-mesa-dev \
    libxrender-dev libxi-dev libxcb1-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-image0-dev libxcb-shm0-dev libx11-xcb-dev libxkbcommon-dev \
    libxkbcommon-x11-dev libxcb-icccm4-dev libxcb-sync0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-randr0-dev libxcb-render-util0-dev libxinerama-dev

# Install mono
RUN apt install -y ca-certificates gnupg && \
    gpg --homedir /tmp --no-default-keyring \
        --keyring /usr/share/keyrings/mono-official-archive-keyring.gpg \
        --keyserver hkp://keyserver.ubuntu.com:80 \
        --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF && \
    echo "deb [signed-by=/usr/share/keyrings/mono-official-archive-keyring.gpg] https://download.mono-project.com/repo/ubuntu stable-focal main" \
        | tee /etc/apt/sources.list.d/mono-official-stable.list && \
    apt update -y && \
    apt install -y mono-complete
